﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TarPit : LevelComponent {

    public string mAnimalTag;
    public float mSlowPercent;

    public void OnTriggerEnter(Collider other) {
        if(other.tag == mAnimalTag) {
            other.gameObject.GetComponent<SphereAi>().SlowSpeed(mSlowPercent/100f);
        }
    }

    public void OnTriggerExit(Collider other) {
        if (other.tag == mAnimalTag) {
            other.gameObject.GetComponent<SphereAi>().ResumeNormalSpeed();
        }
    }

    public override void Initialize() {
        //doesnt need to do anything
    }

    public override void Execute(Action onSuccess = null) {
        //doesnt need to do anything

    }

    public override void End(Action onEnd = null) {
        //doesnt need to do anything

    }
}
