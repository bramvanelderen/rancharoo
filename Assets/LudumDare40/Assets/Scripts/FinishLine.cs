﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class FinishLine : LevelComponent {

    public string mSphereAnimalTag;
    public string mEndLevelName;
    public float mRequiredScoring;

    private float mHerdScoredCount = 0;

    private Action<int> mAddScore;

    public void OnTriggerEnter(Collider other) {

        var sphereAi = other.GetComponent<SphereAi>();
        if (sphereAi == null)
            return;

        sphereAi.IsEnabled = false;
        sphereAi.gameObject.transform.rotation = Quaternion.LookRotation(Vector3.forward * -1);

        if (mAddScore != null)
            mAddScore(1);
    }
    

    public override void AddScoreEvent(Action<int> addScore)
    {
        mAddScore = addScore;
    }

    public override void Initialize()
    {
        //doesnt need to do anything
    }

    public override void Execute(Action onSuccess = null)
    {
        //doesnt need to do anything

    }

    public override void End(Action onEnd = null)
    {
        //doesnt need to do anything

    }
}
