﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Linq;

public class MenuManager : MonoBehaviour {



	// Use this for initialization
	void Start () {
        var results = gameObject.GetComponentsInChildren<Button>();

        var buttonStart = results.FirstOrDefault(x => x.gameObject.name == "Start");
        var buttonEnd = results.FirstOrDefault(x => x.gameObject.name == "Exit");

        buttonStart.onClick.AddListener(StartGame);
        buttonEnd.onClick.AddListener(QuitGame);

        EventManager.OnLoadMenu.AddListener(OpenMenu);
        EventManager.OnGameEnd.AddListener(OpenMenu);
    }
	
	// Update is called once per frame
	void Update () {
		
	}
    
    void StartGame()
    {
        EventManager.OnGameStart.Invoke();
        gameObject.SetActive(false);

    }

    void QuitGame()
    {

        Application.Quit();
    }

    void OpenMenu(int bla, float bla1)
    {
        OpenMenu();
    }

    void OpenMenu()
    {
        gameObject.SetActive(true);
    }

}
