﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Utilities
{
    public static class Extensions
    {
        /// <summary>
        /// Retrieves a quaternion rotation in the direction of a given point
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="lookAt"></param>
        /// <returns></returns>
        public static Quaternion LookRotation(this Transform transform, Vector3 lookAt)
        {
            var dir = (lookAt - transform.position).normalized;

            return Quaternion.LookRotation(dir);
        }

        /// <summary>
        /// Retrieves a quaternion rotation in the direction of a given point
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="x"></param>
        /// <param name="y"></param>
        /// <param name="z"></param>
        /// <returns></returns>
        public static Quaternion LookRotation(this Transform transform, float x, float y, float z)
        {
            var lookAt = new Vector3(x, y, z);
            var dir = (lookAt - transform.position).normalized;

            return Quaternion.LookRotation(dir);
        }

        /// <summary>
        /// TODO DONT USE YET IT IS NOT CORRECT YET
        /// </summary>
        /// <param name="quat0"></param>
        /// <param name="quat1"></param>
        /// <returns></returns>
        public static Quaternion Multiply(this Quaternion quat0, Quaternion quat1)
        {
            long w0 = (long)quat0.w;
            var v0 = new Vector3(quat0.x, quat0.y, quat0.z);

            long w1 = (long)quat1.w;
            var v1 = new Vector3(quat1.x, quat1.y, quat1.z);


            long w2 = (long)(w1 * w0 - Vector3.Dot(v1, v0));
            var v2 = w0 * v1 + Vector3.Cross(v1, v0);
            var result = new Quaternion(v2.x, v2.y, v2.z, w2);

            return result;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="transform"></param>
        /// <param name="target"></param>
        /// <returns></returns>
        public static Vector3 PositionXZ(this Transform transform, Vector3 target)
        {
            var pos = new Vector3(transform.position.x, target.y, transform.position.z);

            return pos;
        }

        /// <summary>
        /// Orbits a certain position around a given center position and axis by a given angle
        /// </summary>
        /// <param name="position"></param>
        /// <param name="center"></param>
        /// <param name="axis"></param>
        /// <param name="angle"></param>
        /// <returns></returns>
        public static Vector3 RotateAround(this Vector3 position, Vector3 center, Vector3 axis, float angle)
        {
            var result = Vector3.zero;
            var rot = Quaternion.AngleAxis(angle, axis); // get the desired rotation
            var dir = position - center; // find current direction relative to center
            dir = rot * dir; // rotate the direction
            result = center + dir; // define new position

            return result;
        }

        /// <summary>
        /// Returns the direction towards the target
        /// </summary>
        /// <param name="current"></param>
        /// <param name="lookAt"></param>
        /// <param name=""></param>
        /// <returns></returns>
        public static Vector3 Direction(this Transform current, Transform lookAt, bool ignoreVerticalDifference = false)
        {
            var result = current.forward;

            if (ignoreVerticalDifference)
            {
                var currentPos = current.position;
                var targetPos = new Vector3(lookAt.position.x, current.position.y, lookAt.position.z);
                result = (targetPos - currentPos).normalized;
            }
            else
            {
                result = (lookAt.position - current.position).normalized;
            }

            return result;
        }

        /// <summary>
        /// Adds an audiosource to a new gameobject that is a child to the given gameobject
        /// </summary>
        /// <param name="component"></param>
        /// <param name="playOnWake"></param>
        /// <returns></returns>
        public static AudioSource AddAudioSource(this GameObject component, bool playOnWake = false)
        {
            var obj = new GameObject();
            obj.transform.parent = component.transform;
            obj.transform.localPosition = Vector3.zero;
            obj.name = "AudioObject";
            var audio = obj.AddComponent<AudioSource>();
            audio.playOnAwake = playOnWake;

            return audio;
        }

        /// <summary>
        /// Checks if an object of type already exists, 
        /// if so that depending on destroy value it will destroy the existing object
        /// </summary>
        /// <param name="comp"></param>
        /// <param name="destroy"></param>
        /// <returns></returns>
        public static bool CheckIfAlreadyExists(this Component comp, bool destroy = false)
        {
            var result = false;
            foreach (var item in GameObject.FindObjectsOfType(comp.GetType()))
            {
                if (item != comp)
                    result = true;
            }

            if (destroy && result)
                Component.Destroy(comp.gameObject);

            return result;
        }
    }
}
