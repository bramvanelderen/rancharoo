﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class AmbientSounds : MonoBehaviour {

    public Range SoundTickRate;
    public CustomClip AmbientClip;

    private float currentTime = 0;
    private AudioSource source;

	// Use this for initialization
	void Start () {
        currentTime = Time.time + SoundTickRate.Random;
        source = gameObject.AddAudioSource();
	}
	
	// Update is called once per frame
	void Update () {
		if (Time.time > currentTime)
        {
            AmbientClip.Play(source);
            currentTime = Time.time + SoundTickRate.Random;
        }
	}
}
