﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

namespace LD40
{
	public class PlayerSound : MonoBehaviour
	{
	    [SerializeField] private CustomClip _step1;
        [SerializeField] private CustomClip _step2;
	    private List<AudioSource> _audio;

        void Start ()
        {
            _audio = new List<AudioSource>();
            for (int i = 0; i < 3; i++)
            {
                _audio.Add(gameObject.AddAudioSource());
            }
        }
		
		void Update () 
		{
			
		}

	    public void Step1()
	    {
	        _step1.Play(_audio[0]);
	    }

	    public void Step2()
	    {
            _step2.Play(_audio[1]);
        }
    }
}

