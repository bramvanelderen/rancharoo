﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace LD40
{
    /// <summary>
    /// StateMachine
    /// </summary>
    public class PlayerStateMachine : MonoBehaviour
    {

        public enum PlayerStates
        {
            Idle,
            Jump,
        }

        private readonly Dictionary<PlayerStates, string> AnimationStateMapper = new Dictionary<PlayerStates, string>()
        {
            {
                PlayerStates.Idle, "Idle"
            },
            {
                PlayerStates.Jump, "Jump"
            }
        };

        public PlayerStates State = PlayerStates.Idle;
        [HideInInspector]
        public float Speed = 1f;

        public bool CanInteract
        {
            get { return State == PlayerStates.Idle; }
        }

        public bool CanMove
        {
            get
            {
                return State == PlayerStates.Idle ||
                       State == PlayerStates.Jump;
            }
        }
        [SerializeField]
        private Animator _anim;

        // Use this for initialization
        void Start()
        {
            SwitchState(PlayerStates.Idle);

        }

        // Update is called once per frame
        void FixedUpdate()
        {
            if (_anim == null)
                return;
            _anim.SetFloat("Speed", Speed);
        }

        public bool SwitchState(PlayerStates state)
        {
            //if (_anim == null)
            //    return false;

            //if (State != state)
            //{
            //    _anim.SetBool(AnimationStateMapper[State], false);
            //    _anim.SetBool(AnimationStateMapper[state], true);
            //    State = state;

                return true;
            //}

            //return false;
        }
    }
}

