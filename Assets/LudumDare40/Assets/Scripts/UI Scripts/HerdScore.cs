﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class HerdScore : LevelComponent {

    public Text mScoreLabel;

    private bool mIsActive = false;

    private void Update() {
        if (mIsActive) {
            int? tempCount = HerdManager.ParticipantsCount;

            if (tempCount != null) {
                mScoreLabel.text = tempCount.ToString();
            }
            else {
                mScoreLabel.text = "0";
            }
        }
       
    }

    public override void Initialize() {
        mScoreLabel.text = "0";
        mIsActive = false;
    }

    public override void Execute(Action onSuccess = null) {
        int? tempCount = HerdManager.ParticipantsCount;

        if (tempCount != null) {
            mScoreLabel.text = tempCount.ToString();
        }
        else {
            mScoreLabel.text = "0";
        }

        mIsActive = true;

    }

    public override void End(Action onEnd = null) {
        mIsActive = false;

    }
}
