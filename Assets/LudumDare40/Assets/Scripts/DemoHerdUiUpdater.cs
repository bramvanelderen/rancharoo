﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class DemoHerdUiUpdater : MonoBehaviour {

    [SerializeField]
    TextMeshPro _text;
    [SerializeField]
    SphereAi _ai;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        _text.text = _ai.herdId.ToString();
	}
}
