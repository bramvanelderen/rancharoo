﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;


public class SoundManager : MonoBehaviour {

    [SerializeField]
    CustomClip _menuTrack;
    [SerializeField]
    CustomClip _gameTrack;
    [SerializeField]
    CustomClip _endGameTrack;

    [SerializeField]
    float _fadeSpeeed = 1f;

    KeyValuePair<SoundState, AudioSource> _sourceMenu;
    KeyValuePair<SoundState, AudioSource> _sourceGame;
    KeyValuePair<SoundState, AudioSource> _sourceEndGame;

    List<KeyValuePair<SoundState, AudioSource>> _sources;

    SoundState _state;

    // Use this for initialization
    void Start () {
        EventManager.OnGameEnd.AddListener(OnGameEnd);
        EventManager.OnLoadMenu.AddListener(OnLoadMenu);
        EventManager.OnGameStart.AddListener(OnGameStart);

        _sourceMenu = new KeyValuePair<SoundState, AudioSource>(SoundState.Menu, gameObject.AddAudioSource());
        _sourceGame = new KeyValuePair<SoundState, AudioSource>(SoundState.Game, gameObject.AddAudioSource());
        _sourceEndGame = new KeyValuePair<SoundState, AudioSource>(SoundState.End, gameObject.AddAudioSource());

        _menuTrack.Play(_sourceMenu.Value);
        _gameTrack.Play(_sourceGame.Value);
        _endGameTrack.Play(_sourceEndGame.Value);

        _sourceMenu.Value.volume = 0f;
        _sourceGame.Value.volume = 0f;
        _sourceEndGame.Value.volume = 0f;

        _sources = new List<KeyValuePair<SoundState, AudioSource>>();
        _sources.Add(_sourceMenu);
        _sources.Add(_sourceGame);
        _sources.Add(_sourceEndGame);

    }

    private void Update()
    {
        foreach(var source in _sources)
        {
            if (source.Key == _state)
            {
                source.Value.volume = Mathf.MoveTowards(source.Value.volume, 1f, _fadeSpeeed * Time.deltaTime);
            } else
            {
                source.Value.volume = Mathf.MoveTowards(source.Value.volume, 0f, _fadeSpeeed * Time.deltaTime);
            }
        }
    }

    void OnGameStart()
    {
        _state = SoundState.Game;

    }

    void OnGameEnd(int dummy, float ha)
    {
        _state = SoundState.End;

    }

    void OnLoadMenu()
    {
        _state = SoundState.Menu;
    }

    enum SoundState
    {
        Menu,
        Game,
        End
    }

}
