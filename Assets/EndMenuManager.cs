﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using System.Linq;

public class EndMenuManager : MonoBehaviour {

    [SerializeField]
    private string _scoreText = "Score: {0}";
    [SerializeField]
    private string _timeText = "Time: {0}";

    private TextMeshProUGUI _scoreTm;
    private TextMeshProUGUI _timeTm;

    // Use this for initialization
    void Start () {

        var tms = gameObject.GetComponentsInChildren<TextMeshProUGUI>();
        _scoreTm = tms.FirstOrDefault(x => x.name == "Score");
        _timeTm = tms.FirstOrDefault(x => x.name == "Time");

        EventManager.OnGameEnd.AddListener(OpenMenu);
        EventManager.OnGameStart.AddListener(CloseMenu);
        gameObject.SetActive(false);

    }



    void OpenMenu(int score, float time)
    {
        _scoreTm.text = string.Format(_scoreText, score.ToString());
        _timeTm.text = string.Format(_timeText, time.ToString());

        gameObject.SetActive(true);
    }

    void CloseMenu()
    {
        gameObject.SetActive(false);
    }

}
