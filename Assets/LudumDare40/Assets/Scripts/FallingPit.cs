﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingPit : MonoBehaviour {

    public string mAnimalSphereTag;

    public void OnTriggerEnter(Collider other) {
        var sphereAi = other.GetComponent<SphereAi>();
        if (sphereAi == null)
            return;

        sphereAi.Death();
    }
}
