﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class TerrainResource : LevelComponent {

    public Slider mSlider;
    public const float mResourceMax = 15f;
    public float mResourcesPerHalfSecond = 2f;

    private float mCurrentResources;
    private bool mCurrentlyPainting;

	// Use this for initialization
	void Start () {
        Reset();

    }

    private void Reset() {
        mCurrentResources = mResourceMax;
        InvokeRepeating("PassiveResourceGain", 1, 0.5f);

        mSlider.maxValue = mResourceMax;
        mSlider.minValue = 0;
    }

    private void PassiveResourceGain() {

        if (!mCurrentlyPainting) {
            mCurrentResources += mResourcesPerHalfSecond;
        }

        if(mCurrentResources > mResourceMax) {
            mCurrentResources = mResourceMax;
        }

        UpdateSliderUI();
    }

    private void UpdateSliderUI() {

        mSlider.value = mCurrentResources;
        
    }

    public void ToggleCurrentlyPainting(bool state) {
        mCurrentlyPainting = state;
    }

    public bool ExpendResource(float amount = 1f) {

        if(mCurrentResources > 0) {
            mCurrentResources--;
            UpdateSliderUI();
            return true;

        }
        else {
            mCurrentlyPainting = true;
            return false;
        }
    }

    public override void Initialize() {
        Reset();
    }

    public override void Execute(Action onSuccess = null) {
        this.enabled = true;

    }

    public override void End(Action onEnd = null) {
        CancelInvoke();
        this.enabled = false;

    }
}
