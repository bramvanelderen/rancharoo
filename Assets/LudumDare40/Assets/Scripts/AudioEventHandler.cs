﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioEventHandler {

    public delegate void TerrainSound(AudioClip clip);
    public static TerrainSound OnPlay;
    public static void PlayTerrainSound(AudioClip clip) {
        if(OnPlay != null) {
            OnPlay(clip);
        }
    }

}
