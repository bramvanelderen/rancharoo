﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class PlayerCreatedLevelComponent : LevelComponent {

    public AudioClip mDestructionClip;

    private void OnDisable() {
        if (mDestructionClip != null) {
            AudioEventHandler.PlayTerrainSound(mDestructionClip);
        }

    }

    public override void Initialize() {
        Destroy(this.gameObject);
    }

    public override void Execute(Action onSuccess = null) {
        //doesnt need to do anything

    }

    public override void End(Action onEnd = null) {
        //doesnt need to do anything

    }
}
