﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class Herd
{
    public int Id;
    public bool active;
    public List<IHerdParticipant> participants;
    private int _minimumParticipants;
    private LayerMask _obstacleMask;

    public Herd(int minimumParticipants, LayerMask obstacleLayer)
    {
        this._obstacleMask = obstacleLayer;
        this._minimumParticipants = minimumParticipants;
        participants = new List<IHerdParticipant>();
        Direction = Vector3.forward;
        _herdDir = Vector3.forward;
    }

    public Vector3 Center { get; private set; }

    public Vector3 Direction { get; set; }
    private Vector3 _herdDir { get; set; }
    private Vector3 _center
    {
        get
        {
            var sum = Vector3.zero;
            participants.ForEach(x => sum += x.Position);

            return sum / participants.Count;
        }
    }

    public bool Scared
    {
        get
        {
            throw new NotImplementedException();
        }
    }

    public void Update()
    {
        if (!active)
            return;

        Center = _center;

        if (participants.Count < _minimumParticipants)
        {
            var targetHerd = HerdManager.NearestHerd(Center, this);
            if (targetHerd != null)
            {
                var tempCenter = new Vector3(Center.x, 1, Center.z);
                var targetCenter = new Vector3(targetHerd.Center.x, 1, targetHerd.Center.z);

                var targetDir = (targetCenter - tempCenter).normalized;
                if (!Physics.Raycast(new Ray(tempCenter + Vector3.up, targetDir), Vector3.Distance(tempCenter, targetCenter), _obstacleMask))
                {
                    Direction = targetDir;
                    return;
                }
            }


            var targetParticipant = HerdManager.NearestParticipant(Center, participants.ToArray());
            if (targetParticipant != null)
            {
                var tempCenter = new Vector3(Center.x, 1, Center.z);
                var targetCenter = new Vector3(targetParticipant.Position.x, 1, targetParticipant.Position.z);

                var targetDir = (targetCenter - tempCenter).normalized;
                if (!Physics.Raycast(new Ray(tempCenter + Vector3.up, targetDir), Vector3.Distance(tempCenter, targetCenter), _obstacleMask))
                {
                    Direction = targetDir;
                    return;
                }
            }
            
            Direction = FindDirection(Direction);

        }
        else
        {
            _herdDir = FindDirection(_herdDir);
            Direction = _herdDir;
        }

        _herdDir = new Vector3(_herdDir.x, 0, _herdDir.z).normalized;
        Direction = new Vector3(Direction.x, 0, Direction.z).normalized;
    }

    private Vector3 FindDirection(Vector3 direction)
    {      

        var rotation = Quaternion.LookRotation(direction);

        var distance = 6f;

        RaycastHit infoMiddle;
        RaycastHit infoLeft;
        RaycastHit InfoRight;

        var origin = Center + Vector3.up;

        var directionLeft = rotation * new Vector3(-.7f, 0, .3f);
        var directionRight = rotation * new Vector3(.7f, 0, .3f);


        bool hitMiddle = Raycast(new Ray(origin, direction), out infoMiddle, distance, _obstacleMask);
        bool hitLeft = Raycast(new Ray(origin, directionLeft), out infoLeft, distance, _obstacleMask);
        bool hitRight = Raycast(new Ray(origin, directionRight), out InfoRight, distance, _obstacleMask);

        if (!hitMiddle)
        {
            return direction;
        }
        else if (!hitLeft || !hitRight)
        {
            var dotLeft = Vector3.Dot(directionLeft, Vector3.forward);
            var dotRight = Vector3.Dot(directionRight, Vector3.forward);

            if (!hitLeft ^ !hitRight)
            {
                return (hitLeft) ? directionRight : directionLeft;
            }
            else
            {
                return (dotLeft >= dotRight) ? directionLeft : directionRight;
            }
        }
        else
        {
            var left = NewDirection(origin, infoLeft, distance);
            var right = NewDirection(origin, InfoRight, distance);
            var middle = NewDirection(origin, infoMiddle, distance);


            var dotLeft = Vector3.Dot(left, Vector3.forward);
            var dotRight = Vector3.Dot(right, Vector3.forward);
            var dotMiddle = Vector3.Dot(middle, Vector3.forward);

            var dict = new List<KeyValuePair<Vector3, float>>();
            dict.Add(new KeyValuePair<Vector3, float>(left, dotLeft));
            dict.Add(new KeyValuePair<Vector3, float>(right, dotRight));
            dict.Add(new KeyValuePair<Vector3, float>(middle, dotMiddle));


            return dict.OrderBy(x => x.Value).FirstOrDefault().Key;
        }
    }

    private Vector3 NewDirection(Vector3 origin, RaycastHit info, float distance)
    {
        var direction = Vector3.zero;
        //Direction = Quaternion.LookRotation(info.normal) * Direction;
        var rh = Quaternion.FromToRotation(Vector3.right, info.normal) * Vector3.forward;
        var lh = Quaternion.FromToRotation(Vector3.left, info.normal) * Vector3.forward;

        var rd = Vector3.Dot(rh, Vector3.forward);
        var ld = Vector3.Dot(lh, Vector3.forward);

        var rhcollission = Raycast(new Ray(origin, rh), out info, distance, _obstacleMask);
        var lhCollission = Raycast(new Ray(origin, lh), out info, distance, _obstacleMask);

        if (rhcollission ^ lhCollission)
        {
            direction = rhcollission ? lh : rh;
        }
        else
        {
            direction = rd > ld ? rh : lh;
        }

        return direction;
    }

    public void Scare(Vector3 point, float radius)
    {

    }

    public void Lure(Vector3 point, float radius)
    {

    }

    private bool Raycast(Ray ray, out RaycastHit info, float distance, LayerMask mask)
    {


        return Physics.BoxCast(ray.origin, new Vector3(1f, .5f, .5f), ray.direction, out info, Quaternion.LookRotation(ray.direction),distance, mask);
        
    }
}