﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Utilities;

public class CreateTerrain : MonoBehaviour {

    public GameObject mTerrainModel;
    public AudioClip mTerrainCreationClip;
    public float mTerrainFinalYLocation = 0;
    public float mTerrainStartingYLocation = -5f;
    public float mLerpTime = 0.2f;


    public Range TriggerAudioTickRate;
    public CustomClip WallAudio;
    private float wallAudioTimer = 0;
    private List<AudioSource> _sources;

    private void Start()
    {
        _sources = new List<AudioSource>();
        _sources.Add(gameObject.AddAudioSource());
        _sources.Add(gameObject.AddAudioSource());
        _sources.Add(gameObject.AddAudioSource());
        _sources.Add(gameObject.AddAudioSource());
    }

    public void CreateAtPoint(Vector3 position) {

        Vector3 tempPosition = position;
        tempPosition.y = mTerrainStartingYLocation;        

        GameObject tempTerrain = GameObject.Instantiate(mTerrainModel,tempPosition, mTerrainModel.transform.rotation);

        //AudioEventHandler.PlayTerrainSound(mTerrainCreationClip);
        StartCoroutine(AnimateTerrain(tempTerrain));


        if (Time.time > wallAudioTimer)
        {
            var source = _sources.Where(x => !x.isPlaying).FirstOrDefault() ?? _sources.FirstOrDefault();
            WallAudio.Play(source);
            wallAudioTimer = Time.time + TriggerAudioTickRate.Random;
        }
    }

    IEnumerator AnimateTerrain(GameObject terrain) {

        Vector3 startingPosition = terrain.transform.position;
        Vector3 endingPosition = terrain.transform.position;
        endingPosition.y = mTerrainFinalYLocation;

        float time = 0.0f;
        float steps = 1 / mLerpTime;

        while(terrain.transform.position.y < endingPosition.y) {

            terrain.transform.position = Vector3.Lerp(startingPosition, endingPosition, time * steps);            

            yield return new WaitForEndOfFrame();

            time += Time.deltaTime;
        }

    }
}
