﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class CollectableSphere : LevelComponent
{
    [SerializeField]
    CustomClip collectedSound;

    public override void End(Action onEnd = null)
    {
        IsEnabled = false;

    }

    public override void Execute(Action onSuccess = null)
    {
        IsEnabled = true;

    }

    public override void LateExecute(Action onSuccess = null)
    {
        _ai.IsEnabled = false;
        HerdManager.RemoveParticipant(_ai);
    }

    public override void Initialize()
    {
        IsEnabled = false;
    }

    [SerializeField]
    private LayerMask _sphereMask;
    [SerializeField]
    private float _tickRate;
    private SphereAi _ai;
    private float _time = 0;

    AudioSource _source;

    // Use this for initialization
    void Start () {
        _ai = GetComponent<SphereAi>();
        _source = gameObject.AddAudioSource();

    }
	
	// Update is called once per frame
	void Update () {
        if (!IsEnabled)
            return;

        if (Time.time <= _time)
            return;

        foreach (var item in Physics.OverlapSphere(transform.position, 5f, _sphereMask))
        {
            if (item.GetComponent<CollectableSphere>() == null)
            {
                _ai.IsEnabled = true;
                IsEnabled = false;
                HerdManager.AddParticipant(_ai);
                collectedSound.Play(_source);

            }
        }

        _time = Time.time + _tickRate;
	}
}
