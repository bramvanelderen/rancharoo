﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class HerdPooler
{
    List<Herd> _herds;

    public HerdPooler(int poolSize, int minParticipants, LayerMask obstacleLayer)
    {
        _herds = new List<Herd>();

        for (int i = 0; i < poolSize; i++)
        {
            var herd = new Herd(minParticipants, obstacleLayer);
            herd.active = false;
            herd.Id = i;
            _herds.Add(herd);
        }
    }

    public Herd GetHerd()
    {
        var herd = _herds.FirstOrDefault(x => !x.active);
        herd.active = true;

        return herd;
    }

    public void DestroyHerd(Herd herd)
    {
        if (!_herds.Contains(herd))
            return;

        while (herd.participants.Count != 0)
        {
            herd.participants[0].herd = null;
            herd.participants.RemoveAt(0);
        }
        herd.active = false;
    }
}