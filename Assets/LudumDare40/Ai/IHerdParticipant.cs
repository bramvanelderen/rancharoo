﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IHerdParticipant
{
    Vector3 Position { get; }
    Herd herd { get; set; }
    void AddToHerdManager();
    void RemoveFromHerdManager();
}
