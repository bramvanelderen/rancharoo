﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Audio;

namespace Utilities
{
    /// <summary>
    /// An audio wrapper which makes it easy to play clips with a variety of custom settings
    /// </summary>
    [Serializable]
    public class CustomClip
    {
        [Header("Clip Settings")]
        public List<AudioClip> Clips = new List<AudioClip> { null };
        public float Volume = 1f;
        public Range Pitch = new Range() { Min = 1f, Max = 1f };
        public Range LowPass = new Range() { Min = 1f, Max = 1f };
        public float Reverb = 1f;
        public bool Loop = false;
        public AudioRolloffMode Rolloff = AudioRolloffMode.Logarithmic;
        public AudioMixerGroup Group;
        public float SpacialBlend = 1f;

        public AudioClip Clip
        {
            get
            {
                if (Clips == null || Clips.Count == 0)
                    return null;

                return Clips[UnityEngine.Random.Range(0, Clips.Count)];
            }
        }

        /// <summary>
        /// Play an audio clip with all the custom settings
        /// </summary>
        /// <param name="audio"></param>
        public void Play(AudioSource audio)
        {
            if (Clip == null)
                return;

            //Apply filters
            var lowPass = audio.gameObject.GetComponent<AudioLowPassFilter>();
            if (!lowPass)
                lowPass = audio.gameObject.AddComponent<AudioLowPassFilter>();
            lowPass.cutoffFrequency = 22000f * LowPass.Random;

            //Apply Audio source settings and play
            audio.Stop();
            audio.clip = Clip;
            audio.volume = Volume;
            audio.pitch = Pitch.Random;
            audio.loop = Loop;
            audio.reverbZoneMix = Reverb;
            audio.rolloffMode = Rolloff;
            audio.outputAudioMixerGroup = Group;
            audio.spatialBlend = SpacialBlend;
            audio.Play();
        }

        public void Play(MonoBehaviour component)
        {
            var audio = component.GetComponent<AudioSource>();
            if (!audio)
                audio = component.gameObject.AddComponent<AudioSource>();

            Play(audio);
        }

        public void Play()
        {
            var obj = new GameObject();
            obj.name = "tempAUDIO";

            var audio = obj.AddComponent<AudioSource>();

            Play(audio);
            GameObject.Destroy(obj, 3f);
        }
    }
}
