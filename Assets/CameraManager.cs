﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class CameraManager : MonoBehaviour {

    [SerializeField]
    Transform _menuTrack;
    [SerializeField]
    Transform _gameTrack;
    [SerializeField]
    Transform _endGameTrack;

    [SerializeField]
    float _smoothTime = .3f;
    [SerializeField]
    float _cameraRotateSpeed = 360f;

    KeyValuePair<SoundState, Transform> _sourceMenu;
    KeyValuePair<SoundState, Transform> _sourceGame;
    KeyValuePair<SoundState, Transform> _sourceEndGame;

    List<KeyValuePair<SoundState, Transform>> _sources;

    SoundState _state;

    Vector3 vel = Vector3.zero;

    // Use this for initialization
    void Start()
    {
        EventManager.OnGameEnd.AddListener(OnGameEnd);
        EventManager.OnLoadMenu.AddListener(OnLoadMenu);
        EventManager.OnGameStart.AddListener(OnGameStart);

        _sourceMenu = new KeyValuePair<SoundState, Transform>(SoundState.Menu, _menuTrack);
        _sourceGame = new KeyValuePair<SoundState, Transform>(SoundState.Game, _gameTrack);
        _sourceEndGame = new KeyValuePair<SoundState, Transform>(SoundState.End, _endGameTrack);

        _sources = new List<KeyValuePair<SoundState, Transform>>();
        _sources.Add(_sourceMenu);
        _sources.Add(_sourceGame);
        _sources.Add(_sourceEndGame);

    }

    private void LateUpdate()
    {
        var target = _sources.Where(x => x.Key == _state).Select(x => x.Value).FirstOrDefault();

        transform.position = Vector3.SmoothDamp(transform.position, target.position, ref vel, _smoothTime);
        transform.rotation = Quaternion.RotateTowards(transform.rotation, target.rotation, _cameraRotateSpeed * Time.deltaTime);
    }

    void OnGameStart()
    {
        _state = SoundState.Game;

    }

    void OnGameEnd(int dummy, float ha)
    {
        _state = SoundState.End;

    }

    void OnLoadMenu()
    {
        _state = SoundState.Menu;
    }

    enum SoundState
    {
        Menu,
        Game,
        End
    }
}
