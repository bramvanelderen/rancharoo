﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using Utilities;
using System.Linq;

public class MousePaintFunction : LevelComponent {

    public CreateTerrain mCreateTerrainFunction;
    public TerrainResource mTerrainResources;
    public string mBaseTerrainTag = "Ground";
    public string mDestructableTerrainTag = "DestructableTerrain";
    public float mTerrainPaintCD = .01f;

    

    private float mLastPaintTime;
    private MouseMode mCurrentMode = MouseMode.None;
    private enum MouseMode{
        None,
        Paint,
        Erase
    };

	// Use this for initialization
	void Start () {
        mLastPaintTime = Time.time - mTerrainPaintCD;
    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetAxis("Fire1") > 0f) {

            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            if(Physics.Raycast(ray, out hit)) {//The 1 is for QueryTriggerInteraction.Ignore
                string tagHit = hit.collider.tag;

                if(mCurrentMode == MouseMode.None) {
                    mTerrainResources.ToggleCurrentlyPainting(true);
                    if (tagHit == mBaseTerrainTag) mCurrentMode = MouseMode.Paint;
                    else if (tagHit == mDestructableTerrainTag) mCurrentMode = MouseMode.Erase;
                }
                

                if (mLastPaintTime + mTerrainPaintCD < Time.time) {
                    if(mCurrentMode == MouseMode.Paint){
                        PaintMode(hit);
                    }else if(mCurrentMode == MouseMode.Erase) {
                        EraseMode(hit.collider);

                    }
                    
                    mLastPaintTime = Time.time;
                }

            }

        }else if(Input.GetAxis("Fire1") == 0f) {
            mTerrainResources.ToggleCurrentlyPainting(false);
            mCurrentMode = MouseMode.None;
        }       

	}

    private void PaintMode(RaycastHit hit) {
        if(hit.collider.tag == "Ground") {
            if (mTerrainResources.ExpendResource()) {
                mCreateTerrainFunction.CreateAtPoint(hit.point);
            }            
        }
       
    }

    private void EraseMode(Collider objToErase) {
        if(objToErase.tag == mDestructableTerrainTag) {
            if (mTerrainResources.ExpendResource()) {
                objToErase.gameObject.SetActive(false);
            }
        }
        
    }

    public override void Initialize() {
        this.enabled = false;
    }

    public override void Execute(Action onSuccess = null) {
        this.enabled = true;

        mLastPaintTime = Time.time - mTerrainPaintCD;
        mCurrentMode = MouseMode.None;

    }

    public override void End(Action onEnd = null) {
        this.enabled = false;

    }
}
