﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class DestructableLevelComponent : LevelComponent {

    public AudioClip mDestructionClip;

    private void OnDisable() {
        if(mDestructionClip != null) {
            AudioEventHandler.PlayTerrainSound(mDestructionClip);
        }
        
    }

    public override void Initialize() {
        this.gameObject.SetActive(true);
    }

    public override void Execute(Action onSuccess = null) {
        //doesnt need to do anything

    }

    public override void End(Action onEnd = null) {
        //doesnt need to do anything

    }
}
