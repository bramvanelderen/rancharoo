﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public abstract class LevelComponent : MonoBehaviour
{
    public bool IsEnabled { get; set; }
    /// <summary>
    /// Implement this in level components that finish the game.
    /// </summary>
    /// <param name="finishGame"></param>
    public virtual void AddScoreEvent(Action<int> addScore) { }

    /// <summary>
    /// Sets the component to its default set up state
    /// </summary>
    public abstract void Initialize();

    /// <summary>
    /// Activates the level component for playing
    /// </summary>
    /// <param name="onSuccess"></param>
    public abstract void Execute(Action onSuccess = null);

    public virtual void LateExecute(Action onSuccess = null) { }

    /// <summary>
    /// Deactivates level component
    /// </summary>
    /// <param name="onEnd"></param>
    public abstract void End(Action onEnd = null);
}

