﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Utilities
{
    /// <summary>
    /// Range class
    /// </summary>
    [Serializable]
    public class Range
    {
        public float Min;
        public float Max;

        /// <summary>
        /// Returns a random value within the min max range
        /// </summary>
        public float Random
        {
            get
            {
                return UnityEngine.Random.Range(Min, Max);
            }
        }
    }
}
