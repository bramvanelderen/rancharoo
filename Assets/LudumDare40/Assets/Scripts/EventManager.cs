﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEngine.Events;

public class UnityEndEvent : UnityEvent<int, float> { }

public static class EventManager
{
        
    public static UnityEvent OnGameStart
    {
        get
        {
            if (_onGameStart == null)
                _onGameStart = new UnityEvent();
            return _onGameStart;
        }
    }

    public static UnityEndEvent OnGameEnd
    {
        get
        {
            if (_onGameEnd == null)
                _onGameEnd = new UnityEndEvent();
            return _onGameEnd;
        }
    }

    public  static UnityEvent OnLoadMenu
    {
        get
        {
            if (_onMenuLoad == null)
                _onMenuLoad = new UnityEvent();
            return _onMenuLoad;
        }
    }

    private static UnityEvent _onGameStart;
    private static UnityEvent _onMenuLoad;
    private static UnityEndEvent _onGameEnd;

}

