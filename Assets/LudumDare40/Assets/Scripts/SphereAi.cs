﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class SphereAi : LevelComponent, IHerdParticipant {

    [SerializeField]
    public int herdId; //FGR TESTING

    [SerializeField]
    public float _baseforce = 20f;
    [SerializeField]
    public float _scaredForceMultiplier = 2f;
    [SerializeField]
    private float _stragglerSpeedMultiplier = 1.4f;
    [SerializeField]
    private float _maxSpeed = 5f;

    [SerializeField]
    private CustomClip _roll;
    [SerializeField]
    private CustomClip _death;

    private AudioSource _source;
    private AudioSource _source2;
    [SerializeField]
    private Range _audioTriggerRate;
    private float _nextSound = 0;


    private float _constMaxSpeed;

    public Vector3 Position { get; set; }

    public Herd herd { get; set; }

    private Vector3 _direction = Vector3.zero;
    private Rigidbody _rb;

    private Vector3 _originalPosition;
    private Quaternion _originalRotation;

    private void Awake()
    {
        _originalPosition = transform.position;
        _originalRotation = transform.rotation;
        _rb = GetComponent<Rigidbody>();
        _constMaxSpeed = _maxSpeed;
        _source = gameObject.AddAudioSource(false);
        _source2 = gameObject.AddAudioSource(false);
    }

    private void OnDestroy()
    {
        RemoveFromHerdManager();
    }

    public void Death()
    {
        IsEnabled = false;
        RemoveFromHerdManager();
        gameObject.SetActive(false);
        _death.Play();
    }

    public void AddToHerdManager()
    {
        HerdManager.AddParticipant(this);
    }

    public void RemoveFromHerdManager()
    {
        HerdManager.RemoveParticipant(this);
    }

    //Percent to slow by
    public void SlowSpeed(float percentageToSlow) 
    {
        _constMaxSpeed = _maxSpeed;
        _maxSpeed *= (1 - percentageToSlow);
    }

    public void ResumeNormalSpeed() 
    {
        _maxSpeed = _constMaxSpeed;
    }

    public void IncreaseMaxSpeed(float newMaxSpeed) 
    {
        if(_maxSpeed < newMaxSpeed) {
            _maxSpeed = newMaxSpeed;
        }
    }
    
	
	// Update is called once per frame
	void Update () {
        Position = transform.position;

        if (!IsEnabled)
            return;

        var force = _baseforce;
        if (herd == null)
        {
            //force *= _stragglerSpeedMultiplier;
            //herdId = -1;

            //var nearestHerd = HerdManager.NearestHerd(transform.position, this.herd);
            //var target = Vector3.forward;
            //if (nearestHerd != null)
            //{
            //    _direction = (nearestHerd.Center - transform.position).normalized;
            //} else
            //{
            //    var nearestParticipant = HerdManager.NearestParticipant(transform.position, this);
            //    if (nearestParticipant != null)
            //    {
            //        _direction = (nearestParticipant.Position - transform.position).normalized;
            //    }
            //}
            force = 0;
        } else
        {
            herdId = herd.Id;
            _direction = herd.Direction;
        }

        _rb.AddForce(_direction * force);

        var vector = new Vector2(_rb.velocity.x, _rb.velocity.z);
        vector = Vector2.ClampMagnitude(vector, _maxSpeed);
        _rb.velocity = new Vector3(vector.x, _rb.velocity.y, vector.y);

        if (Time.time > _nextSound && _rb.velocity.magnitude > 2f)
        {
            _nextSound = _audioTriggerRate.Random + Time.time;
            _roll.Play(_source.isPlaying ? _source2 : _source);
        }
	}

    public override void Initialize()
    {
        RemoveFromHerdManager();
        gameObject.SetActive(true);
        IsEnabled = false;
        transform.position = _originalPosition;
        transform.rotation = _originalRotation;
    }

    public override void Execute(Action onSuccess = null)
    {
        AddToHerdManager();
        
        IsEnabled = true;
        _direction = Vector3.zero;
    }

    public override void End(Action onEnd = null)
    {
        RemoveFromHerdManager();
        IsEnabled = false;
        _rb.velocity = Vector3.zero;
    }
}
