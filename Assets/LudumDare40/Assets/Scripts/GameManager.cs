﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Utilities;

public class GameManager : Singleton<GameManager> {

    public float mPlayerScore;
    public float mGametime = 0;
    
    private List<LevelComponent> _levelComponents;
    
    public GameState GameState { get; private set; }

    [SerializeField]
    CustomClip _addScoreClip;
    private AudioSource _source;

    private void Start()
    {
        _source = gameObject.AddAudioSource();

        Initialize();

        EventManager.OnGameStart.AddListener(StartGame);
    }

    public void Reset() {
        GameState = GameState.Initializing;
        mPlayerScore = 0;
        mGametime = 0;

        _levelComponents.RemoveAll(x => x == null);
        foreach (var comp in GameObject.FindObjectsOfType<LevelComponent>())
        {
            if (_levelComponents.Contains(comp))
                continue;

            _levelComponents.Add(comp);
        }
        _levelComponents.ForEach(x => x.Initialize());
    }

    public void StartGame() {
        if (GameState == GameState.Playing || GameState == GameState.Collecting)
            return;
        

        GameState = GameState.Playing;
        _levelComponents.ForEach(c => c.Execute());
        _levelComponents.ForEach(c => c.LateExecute());
    }

    private void Update() {
        if (GameState == GameState.Playing || GameState == GameState.Collecting) {
            mGametime += Time.deltaTime;

            if (HerdManager.ParticipantsCount == 0 )
            {
                EventManager.OnLoadMenu.Invoke();
                Reset();
            }
        }
    }

    public void FinishGame() {

        GameState = GameState.Done;
        _levelComponents.ForEach(c => c.End());
        EventManager.OnGameEnd.Invoke((int)mPlayerScore, mGametime);
    }

    /// <summary>
    /// Starts the initial state
    /// </summary>
    public void Initialize()
    {
        
        var components = GameObject.FindObjectsOfType<LevelComponent>();
        _levelComponents = new List<LevelComponent>();
        foreach ( var component in components)
        {          
           
            component.AddScoreEvent(AddScore);
            component.Initialize();
            _levelComponents.Add(component);
        }

        GameState = GameState.Initializing;
        mGametime = 0;
        mPlayerScore = 0;
    }

    private void TriggerCollectingState()
    {
        GameState = GameState.Collecting;
        Invoke("FinishGame", 5);
    }

    public void AddScore(int score)
    {
        if (GameState != GameState.Collecting && GameState != GameState.Playing)
            return;

        if (GameState == GameState.Playing)
        {
            TriggerCollectingState();
        }

        mPlayerScore += score;

        _addScoreClip.Play(_source);
    }      

}

public enum GameState
{
    Initializing,
    Playing,
    Collecting,
    Done,
}
