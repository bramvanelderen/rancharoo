﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreationAudioControl : MonoBehaviour {

    public AudioSource mSource;

	// Use this for initialization
	void OnEnable () {
        AudioEventHandler.OnPlay += PlayAudioClip;
	}

    private void OnDisable() {
        AudioEventHandler.OnPlay -= PlayAudioClip;
    }

    private void PlayAudioClip(AudioClip clip) {

        if (mSource.isPlaying) {
            mSource.Stop();
        }

        mSource.clip = clip;
        mSource.Play();

    }
}
