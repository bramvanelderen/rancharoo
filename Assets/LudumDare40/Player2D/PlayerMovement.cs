﻿//using System.Collections;
//using System.Collections.Generic;
//using UnityEngine;
//using Utilities;

//namespace LD40
//{
//    public enum Direction
//    {
//        Left,
//        Right
//    }

//    /// <summary>
//    /// Handles all input for the movement
//    /// </summary>
//    [RequireComponent(typeof(PlayerCollision), typeof(PlayerStateMachine), typeof(Rigidbody2D))]
//    public class PlayerMovement : MonoBehaviour
//    {
//        public bool IsInControl = false;

//        [SerializeField, Range(4f, 20f)]
//        private float _movementSpeed = 10f;
//        [SerializeField, Range(8f, 20f)]
//        private float _jumpSpeed = 10f;
//        [SerializeField]
//        private float _lowJumpMultiplier = 2f;
//        [SerializeField]
//        private float _fallMultiplier = 2.5f;
//        [SerializeField]
//        private float _gravityMultiplier = 2f;
//        [SerializeField]
//        private float _inAirForce = 500f;
//        [SerializeField]
//        private float _wallPushForce = 300f;
//        [SerializeField]
//        private float _slopeFriciton = .5f;
//        private PlayerCollision _collision;
//        private PlayerStateMachine _state;
//        private Rigidbody2D _rb2d;
//        private float _inputWeight = 1f;
//        private Direction _playerDirection = Direction.Right;

//        private bool _isJumping = false;

//        public Direction PlayerDirection
//        {
//            get { return _playerDirection; }
//        }

//        void Start()
//        {
//            _collision = GetComponent<PlayerCollision>();
//            _rb2d = GetComponent<Rigidbody2D>();
//            _state = GetComponent<PlayerStateMachine>();
//        }

//        void Update()
//        {
//            if (!IsInControl)
//                return;

//            if (_isJumping && _collision.IsGrounded)
//            {
//                _isJumping = false;
//                _state.SwitchState(PlayerStateMachine.PlayerStates.Idle);
//            }
//            Jump();


//        }

//        void FixedUpdate()
//        {
//            AdjustInputWeight();

//            if (!_state.CanMove)
//                return;

//            if (!IsInControl)
//            {
//                _state.Speed = 0;
//                return;
//            }
            
//            var horizontal = CustomInput.GetAxis(CustomInput.Axis.LeftStick, CustomInput.Index.Any).x;
//            if (horizontal < -.1)
//            {
//                _playerDirection = Direction.Left;
//                transform.localScale = new Vector3(-1f, 1f, 1f);
//            }
//            else if (horizontal > .1)
//            {
//                _playerDirection = Direction.Right;
//                transform.localScale = new Vector3(1f, 1f, 1f);
//            }
//            _state.Speed = Mathf.Abs(horizontal);

//            var vel = _rb2d.velocity;
//            vel.x = ApplyWeight(vel.x, horizontal * _movementSpeed, _inputWeight);
//            _rb2d.velocity = vel;

//        }

//        void NormalizeSlope()
//        {
//            // Attempt vertical normalization
//            if (_collision.IsGrounded)
//            {
//                RaycastHit2D hit = Physics2D.Raycast(transform.position, -Vector2.up, 1f);

//                if (hit.collider != null && Mathf.Abs(hit.normal.x) > 0.1f)
//                {
//                    Rigidbody2D body = GetComponent<Rigidbody2D>();
//                    // Apply the opposite force against the slope force 
//                    // You will need to provide your own slopeFriction to stabalize movement
//                    body.velocity = new Vector2(body.velocity.x - (hit.normal.x * _slopeFriciton), body.velocity.y);

//                    //Move Player up or down to compensate for the slope below them
//                    Vector3 pos = transform.position;
//                    pos.y += -hit.normal.x * Mathf.Abs(body.velocity.x) * Time.deltaTime * (body.velocity.x - hit.normal.x > 0 ? 1 : -1);
//                    transform.position = pos;
//                }
//            }
//        }

//        void Jump()
//        {
//            if (CustomInput.GetButtonDown(CustomInput.InputButtons.A, CustomInput.Index.Any))
//            {
//                var vel = _rb2d.velocity;
//                var force = Vector3.zero;
//                if (_collision.IsGrounded)
//                {
//                    vel.y = _jumpSpeed;
//                }
//                else if (_collision.WallDetected(PlayerDirection))
//                {
//                    vel.y = _jumpSpeed;
//                    force.x = _wallPushForce * ((PlayerDirection == Direction.Right) ? -1 : 1);
//                    _inputWeight = 0f;
//                }
//                _state.SwitchState(PlayerStateMachine.PlayerStates.Jump);
//                _isJumping = true;

//                _rb2d.velocity = vel;
//                _rb2d.AddForce(force);
//            }


//            if (_rb2d.velocity.y < 0)
//            {
//                _rb2d.velocity += Vector2.up * Physics2D.gravity.y * _gravityMultiplier * (_fallMultiplier - 1) * Time.deltaTime;

//            }
//            else if (_rb2d.velocity.y > 0 && !CustomInput.GetButton(CustomInput.InputButtons.A, CustomInput.Index.Any))
//            {
//                _rb2d.velocity += Vector2.up*Physics2D.gravity.y*_gravityMultiplier*(_lowJumpMultiplier - 1)*
//                                  Time.deltaTime;
//            }
//            else
//            {
//                _rb2d.velocity += Vector2.up*Physics2D.gravity.y*_gravityMultiplier*Time.deltaTime;
//            }
//        }

//        float ApplyWeight(float start, float target, float weight)
//        {
//            var result = start;
//            var difference = Mathf.Abs(start - target);
//            if (target > start)
//            {
//                result += difference * weight;
//            }
//            else
//            {
//                result -= difference * weight;
//            }

//            return result;
//        }

//        void AdjustInputWeight()
//        {
//            if (_inputWeight == 1f)
//                return;
//            _inputWeight += 1f * Time.deltaTime;
//            if (_inputWeight > 1f)
//                _inputWeight = 1f;
//        }

//        public void UpgradeSpeed(float amount)
//        {
//            _movementSpeed += amount;
//        }

//        public void UpgradeJump(float amount)
//        {
//            _jumpSpeed += amount;
//        }
//    }
//}
