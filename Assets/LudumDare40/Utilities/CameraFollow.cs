﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Porcupine
{
	public class CameraFollow : MonoBehaviour
	{
	    [SerializeField] public Transform Target;
	    [SerializeField] private Vector3 _defaultPosition = Vector3.zero;
	    [SerializeField] private Vector3 _offset = new Vector3(0, 2f, -10f);
	    [SerializeField] private float _smoothTime = 0.3f;
	    [SerializeField] private float _zoomOnTarget = 6f;
	    [SerializeField] private float _zoomIdle = 8f;

	    private float _zoom
	    {
	        get { return TargetTransform ? _zoomOnTarget : _zoomIdle; }
	    }
	    public bool TargetTransform = false;

	    private Vector3 _velocity = Vector3.zero;
	    private float _zoomVel = 0f;
        private Camera _camera;

	    void Start()
	    {
	        _camera = GetComponent<Camera>();
	    }
		
		void LateUpdate ()
		{
		    var target = _defaultPosition;
		    if (TargetTransform && Target)
		    {
		        target = Target.position + _offset;
		    }

		    transform.position = Vector3.SmoothDamp(transform.position, target, ref _velocity, _smoothTime);
		    _camera.orthographicSize = Mathf.SmoothDamp(_camera.orthographicSize, _zoom, ref _zoomVel, .3f, 10f);
		}
	}
}

