﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndTileSpeedUp : MonoBehaviour {

    public string mAnimalSphereTag;
    public float mNewMaxSpeed;

    public void OnTriggerEnter(Collider other) {
        if (other.tag == mAnimalSphereTag) {
            other.gameObject.GetComponent<SphereAi>().IncreaseMaxSpeed(mNewMaxSpeed);
        }
    }
}
