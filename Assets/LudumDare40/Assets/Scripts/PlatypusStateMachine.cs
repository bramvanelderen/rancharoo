﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// StateMachine
/// </summary>
public class PlatypusStateMachine : LevelComponent
{

    public enum PlayerStates
    {
        Idle,
        Rol,
        Dance,
    }

    private readonly Dictionary<PlayerStates, string> AnimationStateMapper = new Dictionary<PlayerStates, string>()
    {
            {
                PlayerStates.Idle, "Idle"
            },
            {
                PlayerStates.Rol, "Rol"
            },
        {
            PlayerStates.Dance, "Dance"
        }
    };

    public PlayerStates State = PlayerStates.Idle;
    
    [SerializeField]
    private Animator _anim;

    [SerializeField]
    private float _rotationSpeed;

    [SerializeField]
    private Transform _objectToRotate;

    private Rigidbody _rb;
    private SphereAi _iherd;

    bool _shouldDance = false;

    // Use this for initialization
    void Start()
    {
        _rb = GetComponent<Rigidbody>();
        _iherd = GetComponent<SphereAi>();
        SwitchState(PlayerStates.Idle);

    }

    private void Update()
    {
        if (_anim == null)
            return;
        

        if (_shouldDance)
        {

            SwitchState(PlayerStates.Dance);
        } else
        {
            if (_iherd.herd != null)
            {
                var yRotation = Quaternion.LookRotation(_iherd.herd.Direction).eulerAngles.y;

                var euler = _objectToRotate.rotation.eulerAngles;
                euler = new Vector3(euler.x, yRotation, euler.z);
                _objectToRotate.rotation = Quaternion.Lerp(_objectToRotate.rotation, Quaternion.Euler(euler), Time.deltaTime * _rotationSpeed);
            }            

            //_objectToRotate.Rotate(Vector3.right, _rotationSpeed * Time.deltaTime, Space.Self);

            var result = _rb.velocity.magnitude > .2 ? SwitchState(PlayerStates.Rol) : SwitchState(PlayerStates.Idle);
        }
    }

    public bool SwitchState(PlayerStates state)
    {
        if (_anim == null)
            return false;

        if (State != state)
        {
            _anim.SetBool(AnimationStateMapper[State], false);
            _anim.SetBool(AnimationStateMapper[state], true);
            State = state;
        }

        return true;
    }

    public override void Initialize()
    {
        IsEnabled = false;
        _shouldDance = false;
    }

    public override void Execute(Action onSuccess = null)
    {
        IsEnabled = true;
        _shouldDance = false;
    }

    public override void End(Action onEnd = null)
    {
        IsEnabled = false;
        _shouldDance = true;

    }
}