﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(GameManager))]
public class GameManagerEditor : Editor {

	public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        var script = (GameManager)target;

        GUILayout.Space(10);
        GUILayout.Label("In game stuff");
        GUILayout.Space(5);
        if (script.GameState != GameState.Initializing)
        {
            if (GUILayout.Button("Reset"))
        {
            script.Initialize();
        }
        }
        
        if (script.GameState == GameState.Initializing)
        {
            GUILayout.Space(5);
            if (GUILayout.Button("Start Game"))
            {
                script.StartGame();
            }
            
        }

        if (script.GameState == GameState.Playing || script.GameState == GameState.Collecting)
        {
            GUILayout.Space(5);
            if (GUILayout.Button("End Game"))
            {
                script.FinishGame();
            }

            GUILayout.Space(5);
            if (GUILayout.Button("Add score"))
            {
                script.AddScore(1);
            }

        }

    }

    void IsFinish()
    {
        Debug.Log("Level Finished callback recieved");
    }
}
