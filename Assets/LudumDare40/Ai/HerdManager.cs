﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using UnityEngine;
using Utilities;

public class HerdManager : LevelComponent
{
    [SerializeField, Tooltip("In miliseconds")]
    private int _herdTickRate = 200;
    [SerializeField]
    private float _herdRange = 4f;
    [SerializeField]
    private float _herdInfluenceRange = 10f;
    [SerializeField]
    private int _herdMinimumParticipants = 3;
    [SerializeField]
    private bool _herdUpdaterEnabled = false;
    [SerializeField]
    private LayerMask _obstacleLayer;

    private List<IHerdParticipant> _participants;
    private List<Herd> _herds;
    private Coroutine _herdRoutine;
    private HerdPooler _herdPool;

    private static HerdManager Instance;

    public void Awake()
    {
        Instance = this;
        _participants = new List<IHerdParticipant>();
        _herds = new List<Herd>();
        _herdRoutine = StartCoroutine(HerdUpdater());
        _herdPool = new HerdPooler(100, _herdMinimumParticipants, _obstacleLayer);
    }

    private void OnDestroy()
    {
        StopCoroutine(_herdRoutine);
    }

    private void Update()
    {
        _herds.ForEach(x => x.Update());
    }

    public static void ScareHerds(Vector3 point, float radius)
    {
        if (Instance == null)
            return;

        Instance._herds.ForEach(x => x.Scare(point, radius));
    }

    public static void LureHerds(Vector3 point, float radius)
    {
        if (Instance == null)
            return;
        Instance._herds.ForEach(x => x.Lure(point, radius));
    }
    
    public static Vector3? MainHerdCenter { get; private set; }



    public static int? ParticipantsCount 
    {
        get
        {
            if (Instance == null)
                return null;

            var mainHerd = Instance._herds.OrderByDescending(x => x.participants.Count).FirstOrDefault();

            if (mainHerd == null)
                return null;

            return mainHerd.participants.Count;
        }
    }

    /// <summary>
    /// Retrieves nearby herd within its influence range
    /// </summary>
    /// <param name="point"></param>
    /// <param name="ignoreHerds"></param>
    /// <returns></returns>
    public static Herd NearestHerd(Vector3 point, params Herd[] ignoreHerds)
    {
        if (Instance == null)
            return null;

        return Instance._herds
            .Where(x => Vector3.Distance(x.Center, point) < Instance._herdInfluenceRange && !ignoreHerds.Contains(x))
            .OrderBy(x => Vector3.Distance(x.Center, point)).FirstOrDefault();
    }

    public static IHerdParticipant NearestParticipant(Vector3 point, params IHerdParticipant[] ignoreParticipants)
    {
        if (Instance == null)
            return null;

        return Instance._participants
            .Where(x => Vector3.Distance(x.Position, point) < Instance._herdInfluenceRange && !ignoreParticipants.Contains(x))
            .OrderBy(x => Vector3.Distance(x.Position, point)).FirstOrDefault();
    }

    public static void AddParticipant(IHerdParticipant participant)
    {
        if (Instance == null)
            return;

        if (Instance._participants.Contains(participant))
            return;

        Instance._participants.Add(participant);
    }

    public static void RemoveParticipant(IHerdParticipant participant)
    {
        if (Instance == null)
            return;

        if (!Instance._participants.Contains(participant))
            return;

        Instance._participants.Remove(participant);
        if (participant.herd != null)
        {
            participant.herd.participants.Remove(participant);
            participant.herd = null;
        }
    } 

    #region Herding

    IEnumerator HerdUpdater()
    {
        yield return new WaitForSeconds(_herdTickRate / 1000);

        if (_herdUpdaterEnabled)
        {
            SplitHerds();
            RebuildHerd();
            UpdateCenter();
            CleanHerds();
        }
        _herdRoutine = StartCoroutine(HerdUpdater());
    }

    private void RebuildHerd()
    {      
        //Rebuild herds
        foreach (var participantKey in _participants)
        {
            var herd = participantKey.herd;

            var participantsInRange = _participants.Where(p => Vector3.Distance(p.Position, participantKey.Position) < _herdRange).ToList();

            //If participant is in herd, check if others of the herd are still close
            //If not remove from herd
            if (participantKey.herd != null)
            {
                Herd newHerd = participantsInRange
                    .Where(p => p.herd != null && 
                    p.herd != participantKey.herd && 
                    p.herd.participants.Count >= herd.participants.Count &&
                    !Physics.Raycast(new Ray(participantKey.Position + Vector3.up, p.herd.Center - participantKey.Position), 4, _obstacleLayer))
                    .Select(p => p.herd)
                    .FirstOrDefault();

                if (newHerd == null)
                    continue;

                herd.participants.Remove(participantKey);
                participantKey.herd = newHerd;
                newHerd.participants.Add(participantKey);

                //QUICKFIX to solve removed from the herd problem but still thinks he is in herd
                if (!participantKey.herd.participants.Contains(participantKey))
                {
                    participantKey.herd = null;
                }

                continue;
            }            

            herd = participantsInRange
                .Where(x => x.herd != null && x != participantKey)
                .Select(x => x.herd)
                .FirstOrDefault();

            if (herd == null)
            {
                herd = _herdPool.GetHerd();
                _herds.Add(herd);
            }

            //Recursivly build herd
            GatherHerd(participantKey, herd);            
        }
    }

    private void SplitHerds()
    {
        foreach (var herd in _herds)
        {
            List<List<IHerdParticipant>> groups = new List<List<IHerdParticipant>>();
            var herdPar = new List<IHerdParticipant>(herd.participants);
            
            while (herdPar.Count > 0)
            {
                var par = herdPar[0];
                herdPar.Remove(par);

                groups.Add(GroupHerd(par, herdPar));
            }

            var sorted = groups.OrderBy(x => x.Count);

            for (int i = 0; i < sorted.Count() - 1; i++)
            {
                var group = sorted.ElementAt(i);
                foreach (var p in group)
                {
                    herd.participants.Remove(p);
                    p.herd = null;
                }
            }
        }      
    }

    private List<IHerdParticipant> GroupHerd(IHerdParticipant par, List<IHerdParticipant> herdPar)
    {
        var group = new List<IHerdParticipant>();

        group.Add(par);

        var others = new List<IHerdParticipant> (
            herdPar.Where(x => Vector3.Distance(x.Position, par.Position) < _herdRange && 
            !Physics.Raycast(new Ray(x.Position + Vector3.up, par.Position - x.Position), _herdRange, _obstacleLayer)));

        others.ForEach(x => herdPar.Remove(x));

        foreach(var other in others)
        {
            group.AddRange(GroupHerd(other, herdPar));
        }

        return group;
    }

    private void CleanHerds()
    {
        var emptyHerds = new List<Herd>(_herds.Where(x => x.participants.Count == 0));

        foreach (var herd in emptyHerds)
        {
            _herds.Remove(herd);
            _herdPool.DestroyHerd(herd);
        }

        //var herds = new List<Herd>(_herds);
        //foreach (var herd in herds)
        //{
        //    if (herd.Center.z - MainHerdCenter.GetValueOrDefault().z < -150)
        //    {
        //        while(herd.participants.Count != 0)
        //        {
        //            var participant = herd.participants[0];
        //            participant.herd = null;
        //            herd.participants.Remove(participant);
        //        }
        //        _herds.Remove(herd);
        //        _herdPool.DestroyHerd(herd);
        //    }
        //}
    }

    private void UpdateCenter()
    {

        var oldCenter = MainHerdCenter ?? Vector3.zero;

        Herd mainHerd = null;

        var herds = Instance._herds.Where(x => x.participants.Count > 2);

        if (herds.Count() == 0)
        {
            herds = Instance._herds;
        }

        mainHerd = Instance._herds.OrderBy(x => Vector3.Distance(oldCenter, x.Center)).FirstOrDefault();


        if (mainHerd == null)
        {
            MainHerdCenter = null;
        } else
        {
            MainHerdCenter = mainHerd.Center;
        }

        
    }

    private void GatherHerd(IHerdParticipant participant, Herd herd)
    {
        if (herd == null)
            return;

        herd.participants.Add(participant);
        participant.herd = herd;

        var participantsInRange = _participants.Where(p => Vector3.Distance(p.Position, participant.Position) < _herdRange && p.herd == null);

        foreach (var p in participantsInRange)
        {
            GatherHerd(p, herd);
        }
    }

    public override void Initialize()
    {
        foreach (var herd in _herds)
        {
            herd.active = false;
            herd.Direction = Vector3.forward;
        }
    }

    public override void Execute(Action onSuccess = null)
    {
        foreach(var herd in _herds)
        {
            herd.active = true;
            herd.Direction = Vector3.forward;
        }
    }

    public override void End(Action onEnd = null)
    {
        foreach (var herd in _herds)
        {
            herd.active = false;
            herd.Direction = Vector3.forward;
        }
    }

    #endregion Herding
}




