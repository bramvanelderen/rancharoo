﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CameraHerdFollow: LevelComponent {

    //public float mCorrectionSpeed = 1f;
    public Vector3 Rotation;
    public Vector3 offset;
    

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void LateUpdate () {

        var center = HerdManager.MainHerdCenter;

        if (center == null)
            return;

        if (float.IsNaN(center.Value.x) ||
            float.IsNaN(center.Value.y) ||
            float.IsNaN(center.Value.z))
        {
            center = transform.position;
        }
       
    
        transform.position = center.Value;
        transform.rotation = Quaternion.Euler(Rotation);
        transform.position += transform.rotation * offset;

	}
    public override void Initialize() {
    }

    public override void Execute(Action onSuccess = null) {
        this.enabled = true;

    }

    public override void End(Action onEnd = null) {
        this.enabled = false;

    }
}
